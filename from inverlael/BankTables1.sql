USE bank;

DELETE FROM recurring_payment;

ALTER TABLE recurring_payment
    ADD COLUMN src_account_id INT NOT NULL AFTER branch_id,
    CHANGE account_id dest_account_id INT NOT NULL;

	
	