USE bank;

ALTER TABLE customer
	ADD COLUMN password_enc VARBINARY(255);

UPDATE customer SET password_enc = AES_ENCRYPT(password, 'hger554()*');

ALTER TABLE customer
	DROP COLUMN password;

ALTER TABLE customer
	ADD COLUMN password VARBINARY(255) AFTER username;

UPDATE customer SET password = password_enc;

ALTER TABLE customer
	DROP COLUMN password_enc;
	
ALTER TABLE branch
	ADD COLUMN default_interest_rate INT DEFAULT 0 AFTER max_accounts_per_user;
