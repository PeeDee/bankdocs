/* Script to create or refresh MySQL database for the www.bank.drurys.org */
/*Paul Drury 2012 p.drury@drurys.demon.co.uk*/
USE bank;

DROP TABLE IF EXISTS tran;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS branch;
DROP TABLE IF EXISTS web_session;
DROP TABLE IF EXISTS housekeeping;
DROP TABLE IF EXISTS account_type;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS recurring_payment;
DROP TABLE IF EXISTS recurrence_type;

CREATE TABLE housekeeping
(
    run_date DATE
);

CREATE TABLE web_session
(
	session_key VARCHAR(255),
	name VARCHAR(255),
	item VARCHAR(255),
    date_created DATETIME
);

CREATE TABLE role
(
	role_id INT PRIMARY KEY,
	role_name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE account_type
(
	account_type_id INT,
	account_type_name VARCHAR(255)
);

CREATE TABLE customer
(
    customer_id INT AUTO_INCREMENT PRIMARY KEY,
	branch_id INT DEFAULT 0,
	role_id INT DEFAULT 0,
	username VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL,
	forename VARCHAR(255) NOT NULL,
	surname VARCHAR(255) NOT NULL,
	closed BOOLEAN DEFAULT FALSE,
	trusted_user BOOLEAN DEFAULT TRUE,
    date_created DATETIME
 );
 	
 CREATE TABLE account
(
    account_id INT AUTO_INCREMENT PRIMARY KEY,
    holder_id INT NOT NULL,
	owner_id INT NOT NULL,
	account_type_id INT NOT NULL,
	balance INT NOT NULL,
	account_name VARCHAR(255) DEFAULT 'not named',
	interest_rate INT DEFAULT 0,
	locked INT DEFAULT 0,   /* 0=none, 1=no debits, 2=no credits, 3=all */
	closed BOOLEAN DEFAULT FALSE,
    date_created DATETIME
 );
 
 CREATE TABLE tran
(
    tran_id INT AUTO_INCREMENT PRIMARY KEY,
    account_id INT NOT NULL,
	hidden BOOLEAN DEFAULT FALSE,		/* is this hidden from normal users? */
	reversal_tran_id INT,  				/* the reversing transaction */
	reversed BOOLEAN DEFAULT FALSE,  	/* has this been reversed? */
	tran_type INT DEFAULT 0,			/* 0=transfer, 1=reversal, 2=pocket money 3=interest */
    balancing_tran_id INT, 				/* can be null temporarily */
	amount INT NOT NULL,
	tran_date DATETIME NOT NULL,
	description VARCHAR(255) NOT NULL,
    date_created DATETIME NOT NULL
 );

 CREATE TABLE branch
(
	branch_id INT AUTO_INCREMENT PRIMARY KEY,
	branch_name VARCHAR(255) NOT NULL UNIQUE,
	email VARCHAR(255),
	address_line_1 VARCHAR(255),
	address_line_2 VARCHAR(255),
	address_line_3 VARCHAR(255),
	postcode VARCHAR(255),
	telephone VARCHAR(255),
	max_accounts_per_user INT DEFAULT 5,
	closed BOOLEAN DEFAULT FALSE,
    date_created DATETIME
);

CREATE TABLE recurrence_type
(
    recurrence_type_id INT NOT NULL,
	recurrence_type_name VARCHAR(255) NOT NULL
 );

CREATE TABLE recurring_payment
(
	recurring_payment_id INT AUTO_INCREMENT PRIMARY KEY,
	recurring_payment_name VARCHAR(255) NOT NULL,
	branch_id INT NOT NULL,
    account_id INT NOT NULL,
	amount INT NOT NULL,
	recurrence_type_id INT NOT NULL,
    date_created DATETIME
 );

  /* Values for user roles */
INSERT role	SET role_id = 0, role_name = 'none';
INSERT role SET	role_id = 1, role_name = 'customer';
INSERT role SET	role_id = 2, role_name = 'manager';
INSERT role SET	role_id = 3, role_name = 'administrator';

/* Values for regular payment types */
INSERT recurrence_type SET recurrence_type_id = 1, recurrence_type_name = 'every monday';
INSERT recurrence_type SET recurrence_type_id = 2, recurrence_type_name = '1st of every mmonth';

 /* Insert static info */
 INSERT account_type
	SET
	account_type_id = 0,
	account_type_name = 'Main ledger';

INSERT account_type
	SET
	account_type_id = 1,
	account_type_name = 'Branch ledger';

INSERT account_type
	SET
	account_type_id = 2,
	account_type_name = 'current account';

/* Create head office branch */
INSERT branch SET 
	branch_name = 'Head Office',
	email = 'bank@drurys.demon.co.uk',
	address_line_1 = 'Drumtochty House',
	address_line_2 = 'Harrietfield',
	address_line_3 = 'Perth',
	postcode = 'PH1 3TD',
	telephone = '01738 880223',
    date_created = NOW();

 /* Create the office manager user */
INSERT customer
	SET
		branch_id = 1,
		role_id = 3,
		username = 'bank',
		password = 'b',
		forename = 'bank',
		surname = 'cashier',
		closed = false,
		date_created = NOW();
		
 /* Create the general ledger */
INSERT account
	SET
		holder_id = 1,
		owner_id = 1,
		account_type_id = 0,
		balance = 0,
		date_created = NOW();
