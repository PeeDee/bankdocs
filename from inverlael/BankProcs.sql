/* Script to create or refresh MySQL database for the www.bank.drurys.org */
/*Paul Drury 2012 p.drury@drurys.demon.co.uk*/

USE bank;

DELIMITER $$

/* PROCEDURE postTran - post a credit or debit */
DROP PROCEDURE IF EXISTS postTran $$
CREATE PROCEDURE postTran 
    ( 
		IN _src_account_id INT,
		IN _dest_account_id INT,
		IN _amount INT,
		IN _tran_type INT,
		IN _tran_date DATETIME,
		IN _description VARCHAR(255)
    )
	BEGIN
		DECLARE
			_closedAccounts,
			_src_tran_id,
			_dest_tran_id,
			_src_account_type,
			_src_account_balance INT;

		/* First check neither account is closed*/
		SELECT COUNT(A.account_id) INTO _closedAccounts
		FROM
			account AS A JOIN account AS B
		WHERE (A.account_id = _src_account_id AND B.account_id = _dest_account_id)
			AND (A.closed = TRUE OR B.closed = TRUE);
		
		IF _closedAccounts = 0 THEN
		
			/* Get some details of the source account */
			SELECT account_type_id, balance INTO _src_account_type, _src_account_balance
				FROM account WHERE account_id = _src_account_id;
			
			/* Check the source account has enough funds (or is a ledger) */
			IF _src_account_balance - _amount >= 0 OR _src_account_type = 1 THEN
		
				START TRANSACTION;
				
				/*
					1. create a tran to debit the source account
					2. create a tran credit the destination account
					3. cross-reference the paired transations using balancing_tran_id
					4. update the account balances
				*/
				
				INSERT tran
					SET
						account_id = _src_account_id,
						amount = - _amount,
						tran_date = _tran_date,
						tran_type = _tran_type,
						description = _description,
						date_created = NOW();
				
				SET _src_tran_id = LAST_INSERT_ID();
				
				INSERT tran
					SET
						account_id = _dest_account_id,
						amount = _amount,
						tran_date = _tran_date,
						tran_type = _tran_type,
						balancing_tran_id = _src_tran_id,
						description = _description,
						date_created = NOW();

				SET _dest_tran_id = LAST_INSERT_ID();
				
				UPDATE tran
					SET
						balancing_tran_id = _dest_tran_id
					WHERE tran_id = _src_tran_id;
				
				UPDATE account
					SET
						balance = balance - _amount
					WHERE account_id = _src_account_id;

				UPDATE account
					SET
						balance = balance + _amount
					WHERE account_id = _dest_account_id;
				
				COMMIT;
			END IF;
		END IF;
	END
$$

/* PROCEDURE listBranches - get a list of all branches for display in drop-downs etc*/
DROP PROCEDURE IF EXISTS listBranches $$
CREATE PROCEDURE listBranches ()
    SELECT 
		branch_id,
		branch_name,
		max_accounts_per_user,
		default_interest_rate
	FROM branch
	WHERE closed = FALSE;
$$

/* PROCEDURE getBranch - get branch details*/
DROP PROCEDURE IF EXISTS getBranch $$
CREATE PROCEDURE getBranch
	(
		IN _branch_id INT
	)
    SELECT 
		branch_id,
		branch_name,
		email,
		address_line_1,
		address_line_2,
		address_line_3,
		postcode,
		telephone,
		max_accounts_per_user,
		default_interest_rate,
		date_created
	FROM
		branch
	WHERE
		branch_id = _branch_id
		AND closed = FALSE;
$$

/* PROCEDURE updateBranch - get branch details*/
DROP PROCEDURE IF EXISTS updateBranch $$
CREATE PROCEDURE updateBranch
	(
		IN _branch_id INT,
		IN _branch_name VARCHAR(255),
		IN _email VARCHAR(255),
		IN _address_line_1 VARCHAR(255),
		IN _address_line_2 VARCHAR(255),
		IN _address_line_3 VARCHAR(255),
		IN _postcode VARCHAR(255),
		IN _telephone VARCHAR(255),
		IN _max_accounts_per_user INT,
		IN _default_interest_rate INT
	)
	UPDATE
		branch
    SET 
		branch_name = _branch_name,
		email = _email,
		address_line_1 = _address_line_1,
		address_line_2 = _address_line_2,
		address_line_3 = _address_line_3,
		postcode = _postcode,
		telephone = _telephone,
		max_accounts_per_user = _max_accounts_per_user,
		default_interest_rate = _default_interest_rate
	WHERE
		branch_id = _branch_id;
$$

/* PROCEDURE listCustomers - get a list of all customers for a branch*/
DROP PROCEDURE IF EXISTS listCustomers $$
CREATE PROCEDURE listCustomers
	(
		IN _branch_id INT
	)
    SELECT 
		COUNT(account.balance) AS number_of_accounts,
		SUM(account.balance) AS net_worth,
		customer_id,
        username,
        forename,
        surname,
		branch_id,
		role_id,
		trusted_user,
		role.role_name AS role
	FROM 
		customer JOIN role USING (role_id)
		LEFT JOIN account ON customer.customer_id = account.holder_id
	WHERE 
		customer.branch_id = _branch_id 
		AND customer.closed = FALSE
		AND account.closed = FALSE
	GROUP BY customer.customer_id;
$$


/* PROCEDURE listAllCustomers - get a list of all customers*/
DROP PROCEDURE IF EXISTS listManagers $$
CREATE PROCEDURE listManagers (	)
    SELECT 
		customer_id,
        username,
        forename,
        surname,
		branch_id,
		role_id,
		trusted_user,
		closed,
		role.role_name AS role
	FROM 
		customer JOIN role USING (role_id)
	WHERE 
		role_id = 2;
$$


/* PROCEDURE listRecurringPayments - get a list of all recurring payments*/
DROP PROCEDURE IF EXISTS listRecurringPayments $$
CREATE PROCEDURE listRecurringPayments 
(
	IN _customer_id INT
)
    SELECT 
		/* payment details */
		recurring_payment_id,
		recurring_payment_name,
		amount,
		recurrence_type_id,
		recurrence_type_name,
		recurring_payment.date_created AS date_created,
		
		/* owner details */
		recurring_payment.owner_id AS owner_id,
		E.forename AS owner_forename,
		E.surname AS owner_surname,

		/* source account details */
		src_account_id,
		A.account_name AS src_account_name,
		A.holder_id AS src_account_holder_id,
		C.forename AS src_account_holder_forename,
		C.surname AS src_account_holder_surname,
		
		/* destination account details */
		dest_account_id,
		B.account_name AS dest_account_name,
		B.holder_id AS dest_account_holder_id,
		D.forename AS dest_account_holder_forename,
		D.surname AS dest_account_holder_surname
		
		FROM 
		recurring_payment 
		LEFT JOIN recurrence_type USING (recurrence_type_id)
		LEFT JOIN account AS A ON src_account_id = A.account_id
		LEFT JOIN account AS B ON dest_account_id = B.account_id
		LEFT JOIN customer AS C ON C.customer_id = A.holder_id
		LEFT JOIN customer AS D ON D.customer_id = B.holder_id
		LEFT JOIN customer AS E ON recurring_payment.owner_id = E.customer_id
	WHERE 
		/* customer is either the owner of the payment or is the holder or owner of one of the accounts */
		recurring_payment.owner_id = _customer_id OR
		A.owner_id = _customer_id OR
		B.owner_id = _customer_id OR
		A.holder_id = _customer_id OR
		B.holder_id = _customer_id;
$$

/* PROCEDURE getRecurringPayment - get details of a recurring payments*/
DROP PROCEDURE IF EXISTS getRecurringPayment $$
CREATE PROCEDURE getRecurringPayment 
(
	IN _recurring_payment_id INT
)
    SELECT 
		/* payment details */
		recurring_payment_id,
		recurring_payment_name,
		amount,
		recurrence_type_id,
		recurrence_type_name,
		recurring_payment.date_created AS date_created,
		
		/* owner details */
		recurring_payment.owner_id AS owner_id,
		E.forename AS owner_forename,
		E.surname AS owner_surname,

		/* source account details */
		src_account_id,
		A.account_name AS src_account_name,
		A.holder_id AS src_account_holder_id,
		C.forename AS src_account_holder_forename,
		C.surname AS src_account_holder_surname,
		
		/* destination account details */
		dest_account_id,
		B.account_name AS dest_account_name,
		B.holder_id AS dest_account_holder_id,
		D.forename AS dest_account_holder_forename,
		D.surname AS dest_account_holder_surname
		
		FROM 
		recurring_payment 
		LEFT JOIN recurrence_type USING (recurrence_type_id)
		LEFT JOIN account AS A ON src_account_id = A.account_id
		LEFT JOIN account AS B ON dest_account_id = B.account_id
		LEFT JOIN customer AS C ON C.customer_id = A.holder_id
		LEFT JOIN customer AS D ON D.customer_id = B.holder_id
		LEFT JOIN customer AS E ON recurring_payment.owner_id = E.customer_id
	WHERE 
		/* customer is either the owner of the payment or is the holder or owner of one of the accounts */
		recurring_payment_id = _recurring_payment_id;
$$

/* PROCEDURE authenticateCustomer - check username and password*/
DROP PROCEDURE IF EXISTS authenticateCustomer $$
CREATE PROCEDURE authenticateCustomer 
    ( 
        IN _username VARCHAR(255),
        IN _password VARCHAR(255),
		IN _key VARCHAR(255)
    )
    SELECT 
		role_id
    FROM customer
    WHERE LCASE(username) = LCASE(_username) 
		AND CAST( AES_DECRYPT(password, _key) AS CHAR ) = _password
		AND customer.closed = FALSE; 
$$

/* PROCEDURE getCustomer - get customer details*/
DROP PROCEDURE IF EXISTS getCustomer $$
CREATE PROCEDURE getCustomer 
    ( 
        IN _customer_id INT
    )
    SELECT 
		COUNT(account.account_id) AS number_of_accounts,
		SUM(account.balance) AS net_worth,
        username,
		customer_id,
        forename,
        surname,
		branch_id,
		branch_name,
		role_id,
		trusted_user
	FROM customer LEFT JOIN branch USING (branch_id)
		 LEFT JOIN account ON customer.customer_id = account.holder_id
	WHERE customer_id = _customer_id
		AND customer.closed = FALSE
		AND account.closed = FALSE
	GROUP BY customer.customer_id;
$$

/* PROCEDURE getCustomerFromUsername*/
DROP PROCEDURE IF EXISTS getCustomerFromUsername $$
CREATE PROCEDURE getCustomerFromUsername 
    ( 
        IN _username VARCHAR(255)
    )
	BEGIN
		DECLARE _number_of_accounts INT;
		
		SELECT COUNT(*) INTO _number_of_accounts
		FROM 
			account JOIN customer ON holder_id = customer_id
		WHERE 
			username = _username
			AND account.locked = FALSE
			AND account.closed = FALSE;
		
		SELECT 
			username,
			customer_id,
			forename,
			surname,
			branch_id,
			branch_name,
			role_id,
			trusted_user,
			_number_of_accounts AS number_of_accounts
		FROM 
			customer LEFT JOIN branch USING (branch_id)
		WHERE
			LCASE(username) = LCASE(_username) AND customer.closed = FALSE; 
	END
$$

/* PROCEDURE listAccountsForBranch - get a list of all accounts for display in drop-downs etc*/
DROP PROCEDURE IF EXISTS listAccountsForBranch $$
CREATE PROCEDURE listAccountsForBranch 
	(
		IN _branch_id INT
	)
    SELECT 
        account_id,
		account_name,
		holder_id,
		owner_id,
		forename,
		surname,
		trusted_user,
		balance,
		locked,
		interest_rate
	FROM 
		account JOIN customer ON holder_id = customer_id
	WHERE 
		customer.branch_id = _branch_id
		AND account_type_id = 2 /* 2 = current account */
		AND account.closed = FALSE 
		AND customer.closed = FALSE;
$$
/* PROCEDURE listAccountsForCustomer - get a list of all accounts for display in drop-downs etc*/
DROP PROCEDURE IF EXISTS listAccountsForCustomer $$
CREATE PROCEDURE listAccountsForCustomer 
	(
		IN _customer_id INT
	)
    SELECT 
        account_id,
		account_name,
		holder_id,
		owner_id,
		forename,
		surname,
		trusted_user,
		balance,
		locked,
		interest_rate
	FROM 
		account JOIN customer ON holder_id = customer_id
	WHERE 
		customer.customer_id = _customer_id 
		AND account_type_id = 2 /* 2 = current account */
		AND account.closed = FALSE;
$$


/* PROCEDURE getAccounts - get a list of accounts for this customer*/
DROP PROCEDURE IF EXISTS getAccounts $$
CREATE PROCEDURE getAccounts 
    ( 
        IN _customer_id INT
    )
    SELECT 
        account_id,
		account.account_name,
		holder_id,
		owner_id,
		balance,
		interest_rate,
		locked
	FROM 
		account
    WHERE 
		holder_id = _customer_id 
		AND account_type_id = 2 /* 2 = current account */
		AND closed = FALSE; 
$$

/* PROCEDURE getSessionItem - retrieve an item from session*/
DROP PROCEDURE IF EXISTS getSessionItem $$
CREATE PROCEDURE getSessionItem 
    ( 
        IN _session_key VARCHAR(255),
        IN _name VARCHAR(255)
    )
    SELECT 
		name,
		item
	FROM web_session
    WHERE session_key = _session_key AND name = _name; 
$$

/* PROCEDURE deleteSessionItem - remove an item from session*/
DROP PROCEDURE IF EXISTS deleteSessionItem $$
CREATE PROCEDURE deleteSessionItem 
    ( 
        IN _session_key VARCHAR(255),
        IN _name VARCHAR(255)
    )
    DELETE FROM web_session
    WHERE session_key = _session_key AND name = _name; 
$$

/* PROCEDURE setSessionItem - add or update an item in session*/
DROP PROCEDURE IF EXISTS setSessionItem $$
CREATE PROCEDURE setSessionItem 
    ( 
        IN _session_key VARCHAR(255),
        IN _name VARCHAR(255),
        IN _item VARCHAR(255)
    )
    BEGIN
	
	DELETE FROM web_session
		WHERE session_key = _session_key AND name = _name;

	INSERT web_session
		SET 
			session_key = _session_key,
			name = _name,
			item = _item,
			date_created = NOW();
	END	
$$

/* PROCEDURE createBranch*/
DROP PROCEDURE IF EXISTS createBranch $$
CREATE PROCEDURE  createBranch
    ( 
		IN _branch_name VARCHAR(255),
		IN _email VARCHAR(255),
		IN _address_line_1 VARCHAR(255),
		IN _address_line_2 VARCHAR(255),
		IN _address_line_3 VARCHAR(255),
		IN _postcode VARCHAR(255),
		IN _telephone VARCHAR(255),
		IN _max_accounts_per_user INT,
		IN _default_interest_rate INT,
		OUT _out_branch_id INT,
		OUT _out_message VARCHAR(255)
    )
	BEGIN
	
	DECLARE _exsitng_branch_id INT;
	
	SELECT branch_id INTO _exsitng_branch_id 
		FROM branch
		WHERE LCASE(branch_name) = LCASE(_branch_name);
		
		IF ISNULL(_exsitng_branch_id) THEN
			
			INSERT branch 
				SET
					branch_name = _branch_name,
					email = _email,
					address_line_1 = _address_line_1,
					address_line_2 = _address_line_2,
					address_line_3 = _address_line_3,
					postcode = _postcode,
					telephone = _telephone,
					max_accounts_per_user = _max_accounts_per_user,
					default_interest_rate = _default_interest_rate,
					date_created= NOW();

			SELECT LAST_INSERT_ID() INTO _out_branch_id;
			SELECT 'success' INTO _out_message;
		ELSE	
			SELECT 0 INTO _out_branch_id;
			SELECT 'a branch with this name already exists' INTO _out_message;
		END IF;
	END
$$

	/* PROCEDURE createUser*/
DROP PROCEDURE IF EXISTS createUser $$
CREATE PROCEDURE  createUser
    ( 
		IN _username VARCHAR(255),
		IN _password VARCHAR(255),
		IN _forename VARCHAR(255),
		IN _surname VARCHAR(255),
		IN _branch_id INT,
		IN _role_id INT,
		IN _trusted_user BOOLEAN,
		IN _key VARCHAR(255),
		OUT _out_customer_id INT,
		OUT _out_message VARCHAR(255)
    )
	BEGIN
	
	DECLARE _existing_customer_id INT;
	
	SELECT customer_id INTO _existing_customer_id 
		FROM customer
		WHERE LCASE(username) = LCASE(_username);
		
		IF ISNULL(_existing_customer_id) THEN
			
			INSERT customer 
				SET
					username = LCASE(_username),
					password = AES_ENCRYPT(_password, _key),
					forename = _forename,
					surname = _surname,
					branch_id = _branch_id,
					role_id = _role_id,
					trusted_user = _trusted_user,
					date_created= NOW();

			SELECT LAST_INSERT_ID() INTO _out_customer_id;
			SELECT 'success' INTO _out_message;
		ELSE	
			SELECT 0 INTO _out_customer_id;
			SELECT 'username already exists' INTO _out_message;
		END IF;
	END
$$

/* PROCEDURE addRecurringPayment - add a new recurring payment*/
DROP PROCEDURE IF EXISTS addRecurringPayment $$
CREATE PROCEDURE addRecurringPayment 
( 
	IN _recurring_payment_name VARCHAR(255),
	IN _owner_id INT,
    IN _src_account_id INT,
    IN _dest_account_id INT,
	IN _amount INT,
	IN _recurrence_type_id INT
)
	INSERT recurring_payment
		SET 
			recurring_payment_name = _recurring_payment_name,
			src_account_id = _src_account_id,
			dest_account_id = _dest_account_id,
			owner_id = _owner_id,
			amount = _amount,
			recurrence_type_id = _recurrence_type_id,
			date_created = NOW();
$$

/* PROCEDURE updateRecurringPayment - add a new account*/
DROP PROCEDURE IF EXISTS updateRecurringPayment $$
CREATE PROCEDURE updateRecurringPayment 
( 
	IN _recurring_payment_id INT,
	IN _recurring_payment_name VARCHAR(255),
    IN _src_account_id INT,
    IN _dest_account_id INT,
	IN _amount INT,
	IN _recurrence_type_id INT
)
	UPDATE recurring_payment
		SET 
			recurring_payment_name = _recurring_payment_name,
			src_account_id = _src_account_id,
			dest_account_id = _dest_account_id,
			amount = _amount,
			recurrence_type_id = _recurrence_type_id
		WHERE
			recurring_payment_id = _recurring_payment_id;
$$

/* PROCEDURE deleteRecurringPayment - add a new account*/
DROP PROCEDURE IF EXISTS deleteRecurringPayment $$
CREATE PROCEDURE deleteRecurringPayment 
( 
	IN _recurring_payment_id INT
)
	DELETE FROM recurring_payment
		WHERE
			recurring_payment_id = _recurring_payment_id;
$$

/* PROCEDURE addAccount - add a new account*/
DROP PROCEDURE IF EXISTS addAccount $$
CREATE PROCEDURE addAccount 
    ( 
        IN _holder_id INT,
        IN _owner_id INT,
        IN _account_type_id INT,
		IN _account_name VARCHAR(255),
		IN _interest_rate INT,
		IN _locked INT
    )
	INSERT account
		SET 
			holder_id = _holder_id,
			owner_id = _owner_id,
			account_name = _account_name,
			account_type_id = _account_type_id,
			interest_rate = _interest_rate,
			balance = 0,
			locked = _locked,
			date_created = NOW();
$$

/* PROCEDURE closeAccount - close an account*/
DROP PROCEDURE IF EXISTS closeAccount $$
CREATE PROCEDURE closeAccount 
    ( 
        IN _account_id INT
    )
	BEGIN
		DELETE FROM recurring_payment
			WHERE src_account_id = _account_id OR dest_account_id = _account_id;

		UPDATE account
			SET closed = TRUE
			WHERE account_id = _account_id;
	END
$$

/* PROCEDURE updateCustomer - update a customer*/
DROP PROCEDURE IF EXISTS updateCustomer $$
CREATE PROCEDURE updateCustomer 
	(
		IN _customer_id INT,
		IN _password VARCHAR(255),
		IN _forename VARCHAR(255),
		IN _surname VARCHAR(255),
		IN _trusted_user BOOLEAN,
		IN _key VARCHAR(255)
	)
	BEGIN
		IF _password = '' THEN
			UPDATE customer
				SET 
					forename = _forename,
					surname = _surname,
					trusted_user = _trusted_user
				WHERE
					customer_id = _customer_id;
		ELSE
			UPDATE customer
			SET 
				password = AES_ENCRYPT(_password, _key),
				forename = _forename,
				surname = _surname,
				trusted_user = _trusted_user
			WHERE
				customer_id = _customer_id;
		END IF;
	END
$$
	
/* PROCEDURE updateAccount - udate an account*/
DROP PROCEDURE IF EXISTS updateAccount $$
CREATE PROCEDURE updateAccount 
    ( 
        IN _account_id INT,
		IN _account_name VARCHAR(255),
		IN _interest_rate INT,
		IN _locked INT
    )
	UPDATE account
		SET 
			account_name = _account_name,
			interest_rate = _interest_rate,
			locked = _locked
		WHERE
			account_id = _account_id;
$$

/* PROCEDURE changePassword*/
DROP PROCEDURE IF EXISTS changePassword $$
CREATE PROCEDURE changePassword
	(
		IN _username VARCHAR(255),
		IN _new_password VARCHAR(255),
		IN _key VARCHAR(255)
	) 
	UPDATE customer
		SET
			password = AES_ENCRYPT(_new_password, _key)
		WHERE username = _username;
$$

/* PROCEDURE getAccount - get account details*/
DROP PROCEDURE IF EXISTS getAccount $$
CREATE PROCEDURE getAccount 
    ( 
        IN _account_id INT
    )
    SELECT 
        account_id,
		account.account_name,
		holder_id,
		owner_id,
		balance,
		interest_rate,
		locked
	FROM account
    WHERE account_id = _account_id AND closed = FALSE; 
$$

/* PROCEDURE getLedger - get account details*/
DROP PROCEDURE IF EXISTS getLedger $$
CREATE PROCEDURE getLedger
    ( 
        IN _branch_id INT
    )
	BEGIN
		/* find the branch maanger, the owner of the ledger */
		DECLARE _holder_id INT;
		
		SELECT 
			customer_id INTO _holder_id
		FROM
			customer
		WHERE
			branch_id = _branch_id AND role_id = 2; /* 2 = manager */
	
		SELECT 
			account_id,
			account.account_name,
			holder_id,
			owner_id,
			balance,
			interest_rate,
			locked
		FROM 
			account
		WHERE 
			holder_id = _holder_id AND account_type_id = 1;  /* 1 = branch ledger */
	END
$$

/* PROCEDURE getRecentTrans - get a list of recent transactions for this account*/
DROP PROCEDURE IF EXISTS getRecentTrans $$
CREATE PROCEDURE getRecentTrans 
    ( 
		IN _account_id INT
    )
    SELECT 
		tran_id,
		account_id,
		tran_type,
		reversed,
		reversal_tran_id,
		hidden,
		balancing_tran_id,
		amount,
		tran_date,
		description,
		date_created
    FROM tran 
    WHERE account_id = _account_id AND hidden = FALSE
	ORDER BY tran_date DESC, tran_id DESC
	LIMIT 5; 

$$

/* PROCEDURE getTrans - get a list of all transactions for this account*/
DROP PROCEDURE IF EXISTS getTrans $$
CREATE PROCEDURE getTrans 
    ( 
		IN _account_id INT
    )
    SELECT 
		tran_id,
		account_id,
		tran_type,
		reversed,
		reversal_tran_id,
		hidden,
		balancing_tran_id,
		amount,
		tran_date,
		description,
		date_created
    FROM tran 
    WHERE account_id = _account_id AND hidden = FALSE
	ORDER BY tran_date DESC, tran_id DESC; 
$$

/* PROCEDURE getRecentTransForCustomer - get a list of all transactions for this customer*/
DROP PROCEDURE IF EXISTS getRecentTransForCustomer $$
CREATE PROCEDURE getRecentTransForCustomer 
    ( 
		IN _customer_id INT
    )
    SELECT 
		account.account_name AS account_name,
		amount,
		tran_date,
		description
    FROM account JOIN tran USING (account_id)
    WHERE account.holder_id = _customer_id AND tran.hidden = FALSE
	ORDER BY tran.tran_date DESC, tran.account_id DESC
	LIMIT 20; 
$$

/* PROCEDURE getTransForCustomer - get a list of all transactions for this customer*/
DROP PROCEDURE IF EXISTS getTransForCustomer $$
CREATE PROCEDURE getTransForCustomer 
    ( 
		IN _customer_id INT
    )
    SELECT 
		account.account_name AS account_name,
		amount,
		tran_date,
		description
    FROM account JOIN tran USING (account_id)
    WHERE account.holder_id = _customer_id AND tran.hidden = FALSE
	ORDER BY tran.tran_date DESC, tran.account_id DESC; 
$$


/* PROCEDURE reverseTran - reverse a transaction by posting cancelling transactions */
DROP PROCEDURE IF EXISTS reverseTran $$
CREATE PROCEDURE reverseTran 
    ( 
		IN _tran_id INT
    )
	BEGIN
		
		/*
			1. check the transation is not reversed already
			   check it is not a reversal transaction

			2. post a reversal to the source account
			3. update source as hidden and reversed
			4. update source account balance
			
			5. find the balancing transaction
			
			6. post a reversal to the balancing account
			7. update balancing transaction as hidden and reversed
			8. update balancing transaction account balance
			
			9. cross-reference the paired reversal transations using balancing_tran_id
		*/
		
		DECLARE _now DATETIME;

		DECLARE 
			_src_tran_id, 
			_src_account_id, 
			_src_balancing_tran_id, 
			_src_amount, 
			_src_tran_type INT;
		
		DECLARE 
			_dest_tran_id, 
			_dest_account_id,
			_dest_amount INT;
			
		DECLARE
			_src_closed,
			_dest_closed,
			_src_reversed BOOLEAN;
		
		DECLARE
			_reversal_src_tran_id,
			_reversal_dest_tran_id INT;
		
		SET _now = NOW();
		
		/* find the source transaction */
		SELECT
				tran_id,
				account_id, 
				balancing_tran_id, 
				amount, 
				tran_type,
				reversed
			INTO 
				_src_tran_id, 
				_src_account_id, 
				_src_balancing_tran_id, 
				_src_amount, 
				_src_tran_type, 
				_src_reversed
			FROM tran 
			WHERE tran_id = _tran_id
			LIMIT 1;

		/* find the dest transaction */
		SELECT 
				tran_id, 
				account_id, 
				amount
			INTO 
				_dest_tran_id, 
				_dest_account_id, 
				_dest_amount
			FROM tran WHERE tran_id = _src_balancing_tran_id;

		/* see if the accounts are closed */
		SELECT closed INTO _src_closed
			FROM account
			WHERE account_id = _src_account_id;
			
		SELECT closed INTO _dest_closed
			FROM account
			WHERE account_id = _dest_account_id;
		
		/* IF... the transaction is not reversed already
			and that it is not itself a reversal tran 
			and neither account is closed
		*/
		
		IF NOT _src_reversed 
			AND _src_tran_type <> 1 
			AND NOT _src_closed 
			AND NOT _dest_closed THEN

			START TRANSACTION;
			
			/* ****************** */
			/* SOURCE TRANSACTION */
			/* ****************** */
			
			/* post a reversal to the source account */
			INSERT tran
				SET
					account_id = _src_account_id,
					amount = - _src_amount,
					tran_type = 1, 						/* 1=reversal */
					hidden = TRUE,
					tran_date = _now,
					description = 'reversal',
					date_created = _now;
		
			SET _reversal_src_tran_id = LAST_INSERT_ID();

			/* update source as hidden and reversed */
			UPDATE tran
				SET
					reversed = TRUE,
					hidden = TRUE,
					reversal_tran_id = _reversal_src_tran_id
				WHERE
					tran_id = _src_tran_id;

			/* update source account balance */
			UPDATE account
				SET
					balance = balance - _src_amount
				WHERE
					account_id = _src_account_id;

			/* ****************** */
			/*  DEST TRANSACTION  */
			/* ****************** */
			
			/* post a reversal to the source account */
			INSERT tran
				SET
					account_id = _dest_account_id,
					balancing_tran_id = _reversal_src_tran_id,
					amount = - _dest_amount,
					tran_type = 1, 						/* 1=reversal */
					hidden = TRUE,
					tran_date = _now,
					description = 'reversal',
					date_created = _now;
		
			/* remeber the new tran id for the balancing transaction */
			SET _reversal_dest_tran_id = LAST_INSERT_ID();

			/* update dest tran as hidden and reversed */
			UPDATE tran
				SET
					reversed = TRUE,
					hidden = TRUE,
					reversal_tran_id = _reversal_dest_tran_id
				WHERE
					tran_id = _dest_tran_id;

			/* update dest account balance */
			UPDATE account
				SET
					balance = balance - _dest_amount
				WHERE 
					account_id = _dest_account_id;
			
			/* cross-reference the paired reversal transations using balancing_tran_id */
			UPDATE tran
				SET
					balancing_tran_id = _reversal_dest_tran_id
				WHERE 
					tran_id = _reversal_src_tran_id;
			
			COMMIT;
		
		END IF;
	END
$$

/* PROCEDURE getTran - get details of a transaction */
DROP PROCEDURE IF EXISTS getTran $$
CREATE PROCEDURE getTran 
    ( 
        IN _tran_id INT
    )
    SELECT
		tran_id,
		account_id,
		tran_type,
		reversed,
		reversal_tran_id,
		hidden,
		balancing_tran_id,
		amount,
		tran_date,
		description,
		date_created
    FROM tran 
    WHERE tran_id = _tran_id;

$$

/* PROCEDURE housekeep*/
DROP PROCEDURE IF EXISTS housekeep $$
CREATE PROCEDURE housekeep 
	(
		OUT _out_run_count INT
	)
	BEGIN
		DECLARE _run_date DATETIME;
		DECLARE _loop_count INT DEFAULT 0;
		
		DELETE FROM web_session
			WHERE DATEDIFF(NOW(), date_created) > 1;
		
		/* find the date the  housekeeing was last run */
		SELECT MAX(run_date) INTO _run_date
			FROM housekeeping;
			
		IF NOT ISNULL(_run_date) THEN
			
			loopLabel: LOOP
			
				/* if we are up to date leave the loop */
				IF DATEDIFF(NOW(), _run_date) < 1 THEN
					LEAVE loopLabel;
				END IF;
				
				/* move on to the following day */
				SET _run_date = DATE_ADD(_run_date, INTERVAL 1 DAY);

				/* mark the day as having been attempted */
				INSERT housekeeping
					SET	run_date = _run_date;
				
				/* if first of month pay interest and recurring payments*/
				IF  DAYOFMONTH(_run_date) = 1 THEN
					CALL payInterest(_run_date);
					CALL payRecurringPayments(2, _run_date); /*2=pay monthly */
				END IF;
		
				/* if monday (sunday=1, monday=2) pay recurring payment */ 
				IF  DAYOFWEEK(_run_date) = 2 THEN
					CALL payRecurringPayments(1, _run_date); /*1=pay weekly */
				END IF;
				
				SET _loop_count = _loop_count + 1;
				
				ITERATE loopLabel;
			
			END LOOP loopLabel;
			
		END IF;

		SET _out_run_count = _loop_count;
	END
$$

/* PROCEDURE payInterest*/
DROP PROCEDURE IF EXISTS payInterest $$
CREATE PROCEDURE payInterest
	(
		_run_date DATE
	)
	BEGIN
	
	DECLARE 
		_account_id , 
		_interest_rate,
		_balance,
		_tran_count INT;
	
	DECLARE 
		done INT DEFAULT FALSE;
	
	/* cursor to get a list of all regular accounts */
	DECLARE account_cursor CURSOR FOR 
		SELECT account_id, interest_rate 
		FROM account
		WHERE account_type_id = 2 AND interest_rate <> 0 AND NOT closed;

	DECLARE CONTINUE HANDLER FOR NOT FOUND 
		SET done = TRUE;
	
	OPEN account_cursor;
	
	read_loop: LOOP

		FETCH account_cursor INTO _account_id, _interest_rate;
	    
		IF done THEN 
			LEAVE read_loop;
		END IF;

		/* see if there is an interest transaction already posted (3=interest transaction) */
		SELECT COUNT(tran_id) INTO _tran_count
			FROM tran
			WHERE account_id = _account_id 
			AND tran_type = 3
			AND reversed = false
			AND DATEDIFF(_run_date, tran_date) = 0;

		IF _tran_count = 0 THEN
			
			/* find the balance at the run date by summing previous transactions, 
			/* exclude reversed transactions and reversals (1=reversal transaction)*/
			SELECT SUM(amount) INTO _balance
				FROM tran
				WHERE account_id = _account_id 
				AND reversed = false
				AND tran_type <> 1
				AND DATEDIFF(_run_date, tran_date) > 0;
			
			/* post the transction: 1=ledger account and 3=interst transaction type */
			CALL postTran(1, _account_id, ROUND((_balance * _interest_rate) / 100), 3, _run_date, 'interest');
		
		END IF;
	END LOOP;

	CLOSE account_cursor;
	
	END
$$

/* PROCEDURE payRecurringPayments*/
DROP PROCEDURE IF EXISTS payRecurringPayments $$
CREATE PROCEDURE payRecurringPayments
	(
		IN _recurrence_type INT,
		IN _run_date DATE
	)
	BEGIN

		/* get the list of recurring payments to process into a cursor*/
		DECLARE done INT DEFAULT FALSE;
		DECLARE _src_account_id INT;
		DECLARE _dest_account_id INT;
		DECLARE _amount INT;
		DECLARE _recurring_payment_name VARCHAR(255);
		DECLARE payment_cursor CURSOR FOR 
			SELECT src_account_id, dest_account_id, amount, recurring_payment_name
			FROM recurring_payment
			WHERE recurrence_type_id = _recurrence_type;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
		
		OPEN payment_cursor;
		
		read_loop: LOOP
		
			FETCH payment_cursor INTO _src_account_id, _dest_account_id, _amount, _recurring_payment_name;
			
			IF done THEN 
				LEAVE read_loop;
			END IF;

			/* 0=transfer */
			CALL postTran(_src_account_id, _dest_account_id, _amount, 0, _run_date, _recurring_payment_name);
		
		END LOOP;

		CLOSE payment_cursor;
	END
$$

/* PROCEDURE closeCustomer - delete a customer*/
DROP PROCEDURE IF EXISTS closeCustomer $$
CREATE PROCEDURE closeCustomer 
    ( 
        IN _customer_id INT
    )
	BEGIN
	
		/* close all accounts for this customer */
		DECLARE done INT DEFAULT FALSE;
		DECLARE _account_id INT;
		DECLARE account_cursor CURSOR FOR 
			SELECT account_id
			FROM account
			WHERE holder_id = _customer_id;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
		
		OPEN account_cursor;
		
		read_loop: LOOP
		
			FETCH account_cursor INTO _account_id;
			
			IF done THEN 
				LEAVE read_loop;
			END IF;

			CALL closeAccount(_account_id);
			
		END LOOP;
		CLOSE account_cursor;		
		
		/* now close the customer */
		UPDATE customer
			SET closed = TRUE
			WHERE customer_id = _customer_id;
	END
$$

/* PROCEDURE closeBranch - delete a branch*/
DROP PROCEDURE IF EXISTS closeBranch $$
CREATE PROCEDURE closeBranch 
    ( 
        IN _branch_id INT
    )
	BEGIN
	
		/* close all customers for this branch */
		DECLARE done INT DEFAULT FALSE;
		DECLARE _customer_id INT;
		DECLARE customer_cursor CURSOR FOR 
			SELECT customer_id
			FROM customer
			WHERE branch_id = _branch_id;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
		
		OPEN customer_cursor;
		
		read_loop: LOOP
		
			FETCH customer_cursor INTO _customer_id;
			
			IF done THEN 
				LEAVE read_loop;
			END IF;

			CALL closeCustomer(_customer_id);
			
		END LOOP;
		CLOSE customer_cursor;		
		
		/* now close the customer */
		UPDATE branch
			SET closed = TRUE
			WHERE branch_id = _branch_id;
	END
$$
